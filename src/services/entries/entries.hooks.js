const { authenticate } = require('feathers-authentication').hooks
const { iff, populate, serialize } = require('feathers-hooks-common')
const { forOwn, groupBy, uniq } = require('lodash')
const {time, score} = require('htr-utils');
const moment = require('moment')
require('moment-round')

const fleetRelation = {
  include: {
    service: 'fleets',
    nameAs: 'fleet',
    parentField: 'fleet_id',
    childField: '_id'
  }
}

const classRelation = {
  include: {
    service: 'classes',
    nameAs: 'class',
    parentField: 'class_id',
    childField: '_id'
  }
}

// TODO avoid running computeTimes repeatedly; maybe put the computed values into hook for quick access?
const serializeScheme = {
  computed: {
    startTime: (item, hook) => getComputedTimes(item, hook).startTime,
    sailingTime: (item, hook) => getComputedTimes(item, hook).sailingTime,
    finlysSailingTime: (item, hook) => getComputedTimes(item, hook).finlysSailingTime,
    avgSpeed: (item, hook) => getComputedTimes(item, hook).avgSpeed
  }
  // computed: {
  //   startTime: (item, hook) => computeTimes(item, hook).startTime,
  //   sailingTime: (item, hook) => computeTimes(item, hook).sailingTime,
  //   finlysSailingTime: (item, hook) => computeTimes(item, hook).finlysSailingTime,
  //   avgSpeed: (item, hook) => computeTimes(item, hook).avgSpeed
  // }

}

/**
* Compute times and average speed if finishTime exist
* @return {Object} computed
*/
const computeTimes = (item, hook) => {
  let computed = {}
  if (!hook.includeStartTimes) return computed
  let myclass = hook.classes[item.class_id]

  switch (myclass.start) {
    case 'finlys':
      let first = myclass.startTime
      let lys = item.handicap['finlys']
      let min = myclass.minHandicap['finlys']
      let route = myclass.routeLength
      computed.startTime = time.finlysStart(first, lys, min, route)
      break
    default:
      computed.startTime = (item.fleet ? item.fleet.startTime : false) || myclass.startTime
      break
  }

  if (!item.finishTime) return computed

  computed.sailingTime = time.sailing(computed.startTime, item.finishTime)
  if (myclass.handicap.indexOf('finlys') !== -1)
    computed.finlysSailingTime = time.sailing(myclass.startTime, item.finishTime)

  computed.avgSpeed = score.avgSpeed(computed.sailingTime, myclass.routeLength)

  return computed
}

/**
* Hook computedTimes
*/
const hookComputedTimes = () => hook => {
  let entries = getEntries(hook)
  let computedTimes = {}
  entries.forEach(item => {
    computedTimes[item._id] = computeTimes(item, hook)
  })
  hook.computedTimes = computedTimes
  return hook
}

/**
* Hook race classes when necessary
*/
const hookClasses = () => hook => {
  let entries = getEntries(hook)

  let classIDs = uniq(entries.map(item => item.class_id))

  return hook.app.service('classes')
    .find({
      query: {
        _id: {
          $in: classIDs
        },
        minHandicap: 'include',
        startTimes: 'exclude' // exclude startTimes in inner populate to avoid recursion
      }
    })
    .then(result => {
      hook.classes = result.data.reduce((acc, obj) => {
        acc[obj._id] = obj
        return acc
      }, {})
      return hook
    })
}


/**
* Simple way to get computed times from the hook
*/
const getComputedTimes = (item, hook) => {
  return hook.computedTimes ? hook.computedTimes[item._id] : {}
}

/**
* Simple helper to get array of entries
*/
const getEntries = hook => {
  return Array.isArray(hook.result)
    ? hook.result
    : Array.isArray(hook.result.data)
      ? hook.result.data
      : [hook.result]
}

/**
* Populate hook.includeStartTimes: choose whether the start times should be included (true unless explicitly excluded)
*/
const includeStartTimes = () => hook => {
  hook.includeStartTimes = hook.params.query.startTimes !== 'exclude'
  delete hook.params.query.startTimes
}

module.exports = {
  before: {
    all: [authenticate('jwt'), includeStartTimes()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [
      populate({schema: classRelation}),
      populate({schema: fleetRelation}),
      iff(hook => hook.includeStartTimes, hookClasses(), hookComputedTimes()),
      serialize(serializeScheme)
    ],
    get: [
      populate({schema: classRelation}),
      populate({schema: fleetRelation}),
      iff(hook => hook.includeStartTimes, hookClasses(), hookComputedTimes()),
      serialize(serializeScheme)
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
