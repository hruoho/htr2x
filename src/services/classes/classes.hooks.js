const { authenticate } = require('feathers-authentication').hooks;
const { iff, populate, serialize } = require('feathers-hooks-common');
const { min } = require('lodash');
const moment = require('moment');

const entrySchema = (query) => ({
  include: {
    service: 'entries',
    nameAs: 'entries',
    parentField: '_id',
    childField: 'class_id',
    asArray: true,
    query: query,
    innerPopulate: true
  }
})

const serializeSchema = {
  computed: {
    /**
    * Find minimum for each item.handicap
    */
    minHandicap: (item, hook) => {
      if (!hook.includeMinHandicap) return
      return item.handicap.reduce((acc, hnd) => {
        let handicaps = item.entries.map(entry => parseFloat(entry.handicap[hnd])) // too naive?
        acc[hnd] = min(handicaps)
        return acc
      }, {})
    },
    startTime: (item, hook) => {
      if (item.startTime) return moment.utc(item.startTime)
    }
  }
}

const includeMinHandicap = () => hook => {
  hook.includeMinHandicap = hook.params.query.minHandicap === 'include'
  delete hook.params.query.minHandicap
}

const includeEntries = () => hook => {
  hook.includeEntries = hook.params.query.entries !== 'exclude'
  delete hook.params.query.entries
}

const includeStartTimes = () => hook => {
  hook.includeStartTimes = hook.params.query.startTimes !== 'exclude'
  hook.includeStartTimesOrig = hook.params.query.startTimes || 'include'
  console.log('stimes', hook.includeStartTimesOrig);
  delete hook.params.query.startTimes
}

module.exports = {
  before: {
    all: [ authenticate('jwt'), includeMinHandicap(), includeEntries(), includeStartTimes() ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [
      iff (hook => hook.includeEntries, populate({schema: hook => entrySchema({startTimes: hook.includeStartTimesOrig})})),
      serialize(serializeSchema),
    ],
    get: [
      iff (hook => hook.includeEntries, populate({schema: hook => entrySchema({startTimes: hook.includeStartTimesOrig})})),
      serialize(serializeSchema),
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
