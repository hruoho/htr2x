// Initializes the `fleets` service on path `/fleets`
const createService = require('feathers-nedb');
const createModel = require('../../models/fleets.model');
const hooks = require('./fleets.hooks');
const filters = require('./fleets.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'fleets',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/fleets', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('fleets');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
