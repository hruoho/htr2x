const { authenticate } = require('feathers-authentication').hooks;
const { populate } = require('feathers-hooks-common');

const entrySchema = {
  include: {
    service: 'entries',
    nameAs: 'entries',
    parentField: '_id',
    childField: 'fleet_id',
    query: {
      startTimes: 'exclude',
    }
  }
};

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [
      populate({ schema: entrySchema })
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
