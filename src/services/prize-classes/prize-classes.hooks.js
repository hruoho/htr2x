const { authenticate } = require('feathers-authentication').hooks
const { populate, serialize } = require('feathers-hooks-common')
const { correction, score, time } = require('htr-utils')
const { orderBy } = require('lodash')
const moment = require('moment')
require('moment-round')

const filters = {
  isLadyCrew: {
    isLadyCrew: true
  },
  isBusiness: {
    isBusiness: true
  }
}

/**
* Custom selector to handle all the fuzzyness here
*/
const select = (hook, parent, depth) => {
  let query = {}
  let selector = {}
  if (parent.fleets) {
    selector = {
      fleet_id: {
        $in: parent.fleets
      }
    }
  } else if (parent.classes) {
    selector = {
      class_id: {
        $in: parent.classes
      }
    }
  } else {
    selector = {
      race_id: parent.race_id
    }
  }

  if (parent.filters) {
    parent.filters.forEach(fil => {
      selector = Object.assign(selector, filters[fil])
    })
  }

  query = Object.assign(query, selector)
  return query
}

const entrySchema = (query) => ({
  include: {
    service: 'entries',
    nameAs: 'entries',
    asArray: true,
    query: query,
    select: select,
    useInnerPopulate: true
  }
})

const serializeSchema = {}

const sortEntries = () => hook => {
  const sorted = prizeClass => {
    prizeClass.entries = orderBy(prizeClass.entries, prizeClass.orderBy)
    return prizeClass
  }

  if (Array.isArray(hook.result)) {
    hook.result = hook.result.map(item => sorted(item))
  } else if (Array.isArray(hook.result.data)) {
    hook.result = hook.result.data.map(item => sorted(item))
  } else hook.result = sorted(hook.result)
}

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [populate({schema: entrySchema({startTimes: 'include'})})],
    get: [populate({schema: entrySchema({startTimes: 'include'})}), serialize(serializeSchema), sortEntries()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
