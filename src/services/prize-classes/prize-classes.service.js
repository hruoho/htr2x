// Initializes the `prizeClasses` service on path `/prize-classes`
const createService = require('feathers-nedb');
const createModel = require('../../models/prize-classes.model');
const hooks = require('./prize-classes.hooks');
const filters = require('./prize-classes.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'prize-classes',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/prize-classes', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('prize-classes');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
