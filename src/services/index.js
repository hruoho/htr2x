const users = require('./users/users.service.js');
const entries = require('./entries/entries.service.js');
const races = require('./races/races.service.js');
const classes = require('./classes/classes.service.js');
const fleets = require('./fleets/fleets.service.js');
const prizeClasses = require('./prize-classes/prize-classes.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(users);
  app.configure(entries);
  app.configure(races);
  app.configure(classes);
  app.configure(fleets);
  app.configure(prizeClasses);
};
