const rp = require('request-promise')
const lodash = require('lodash')
const moment = require('moment')
let jwt = ''

rp({
  method: 'POST',
  uri: 'http://localhost:3030/authentication',
  body: {
    strategy: 'local',
    email: 'hru@fastmail.com',
    password: 'salasana'
  },
  json: true
})
.then(data => {
  jwt = data.accessToken
  return rp({
    method: 'DELETE',
    uri: 'http://localhost:3030/entries',
    headers: {
      'Authorization': jwt
    },
    json: true
  })
})
.then(() => setGotEntries())
.catch(function (err) {
  console.log(err)
})

function setGotEntries () {
  return rp({
    uri: 'https://entry.helsinkitallinnarace.fi/api/race/1/entry',
    json: true
  })
  .then(function (data) {
    data = data.entries.map(transform)
    return rp({
      method: 'POST',
      uri: 'http://localhost:3030/entries',
      body: data,
      json: true, // Automatically stringifies the body to JSON,
      headers: {
        'Authorization': jwt
      }
    })
  })
}

// notice that object spread needs at least node v8.3
function transform (ent) {
  return {
    _id: ent.id,
    boat_name: ent.boat.name,
    boat_type: ent.boat.type,
    class_id: resolveClassID(ent),
    fleet_id: resolveFleetID(ent),
    race_id: 'example',
    handicap: resolveHandicap(ent),
    sailnumber: ent.boat.sailnumber,
    isLadyCrew: !!ent.ladycrew,
    isBusiness: !!ent.isBusiness,
    finishTime: ent.finishTime.length > 3 ? moment.utc('2017-08-19T' + ent.finishTime) : null,
    ...lodash.pick(ent, ['club', 'crew', 'isFirstTimer', 'penalty', 'penaltyInfo', 'pictureUrl', 'startLabel'])
  }
}

function resolveHandicap (ent) {
  let hnd = {}
  ent.handicap = parseFloat(ent.handicap)
  ent.handicapDiv = parseFloat(ent.handicapDiv)
  if (ent.handicap.toString() === ent.handicap.toFixed(3)) hnd.irc_tcc = ent.handicap
  else if (ent.handicap < 2) { hnd.finlys = ent.handicap }
  else if (ent.handicapDiv !== ent.handicap) {
    hnd.orc_tod_off = ent.handicap
    hnd.orc_cdl = ent.handicapDiv
  }
  return hnd
}

function resolveClassID (ent) {
  let handicap = resolveHandicap(ent)
  if (ent.raceClass === 'First 31.7') return 'example-4'
  if (handicap.finlys && !ent.isBusiness) return 'example-0'
  if (handicap.finlys) return 'example-1'
  if (handicap.orc_tod_off) return 'example-2'
  if (handicap.irc_tcc) return 'example-3'
}

function resolveFleetID (ent) {
  let fleets = {
    'LYS 1': 'example-0-0',
    'LYS 2': 'example-0-1',
    'LYS 3': 'example-0-2',
    'LYS 4': 'example-0-3',
    'Business LYS': 'example-1-0',
    'ORC 1': 'example-2-0',
    'ORC 2': 'example-2-1',
    'IRC': 'example-3-0',
    'First 31.7': 'example-4-0'
  }
  let fleet = fleets[ent.raceClass]
  if (!fleet) console.log('Fleet not found', ent.raceClass)
  return fleet
}
