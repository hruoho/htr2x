const assert = require('assert');
const app = require('../../src/app');

describe('\'prizeClasses\' service', () => {
  it('registered the service', () => {
    const service = app.service('prize-classes');

    assert.ok(service, 'Registered the service');
  });
});
