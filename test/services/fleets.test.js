const assert = require('assert');
const app = require('../../src/app');

describe('\'fleets\' service', () => {
  it('registered the service', () => {
    const service = app.service('fleets');

    assert.ok(service, 'Registered the service');
  });
});
